# postfix

Docker container for postfix relay.

Adapted from:

- https://github.com/wader/postfix-relay
- https://github.com/tozd/docker-postfix
- https://github.com/tomav/docker-mailserver

Here are some things to be aware of.

- To set the alias for `root`, use the environment variable `ROOT_ALIAS`.
- All environment variables beginning with POSTFIX_ will be applied via
  `postconf -e` (where the variable name is what follows the POSTFIX_).
