FROM debian:buster-slim

MAINTAINER rgrempel@cmu.ca

COPY ./etc/aliases /etc/aliases

ENV \
  POSTFIX_myhostname=hostname \
  POSTFIX_mydestination=localhost \
  POSTFIX_mynetworks=127.0.0.1/8 \
  POSTFIX_smtp_tls_security_level=may \
  POSTFIX_smtpd_tls_security_level=none

RUN \
  echo postfix postfix/main_mailer_type string "'Satellite system'" | debconf-set-selections && \
  echo postfix postfix/mynetworks string "127.0.0.1/8" | debconf-set-selections && \
  echo postfix postfix/mailname string temporary.example.com | debconf-set-selections && \
  apt-get update && \
  apt-get -y --no-install-recommends install \
    procps \
    postfix \
    ca-certificates \
    rsyslog && \
  apt-get clean && rm -rf /var/lib/apt/lists/*

COPY ./etc /etc
COPY run /root/

CMD ["/root/run"]
